import React from 'react';

import linkedImage from './linked-image.twig';
import linkedImageData from './linked-image.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Linked Images' };

export const image = () => (
  <div dangerouslySetInnerHTML={{ __html: linkedImage(linkedImageData) }} />
);
