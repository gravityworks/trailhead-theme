import React from 'react';

import cta from './cta.twig';

import ctaData from './cta.yml';
import shapedBackgroundData from './shaped-background.yml';
import slantedBackgroundData from './slanted-background.yml';
import windowshadeData from './windowshade.yml';
import linedTextHeadingData from './lined-text-heading.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/CTAs' };

export const ctaExample = () => (
  <div dangerouslySetInnerHTML={{ __html: cta(ctaData) }} />
);
export const shapedBackgroundCTA = () => (
  <div dangerouslySetInnerHTML={{ __html: cta(shapedBackgroundData) }} />
);
export const slantedBackgroundCTA = () => (
  <div dangerouslySetInnerHTML={{ __html: cta(slantedBackgroundData) }} />
);
export const windowshadeCTA = () => (
  <div dangerouslySetInnerHTML={{ __html: cta(windowshadeData) }} />
);
export const linedTextHeadingCTA = () => (
  <div dangerouslySetInnerHTML={{ __html: cta(linedTextHeadingData) }} />
);
