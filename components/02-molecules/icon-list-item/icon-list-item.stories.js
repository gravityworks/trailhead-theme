import React from 'react';

import iconListItem from './icon-list-item.twig';
import iconListItemData from './icon-list-item.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Icon List Item' };

export const listItem = () => (
  <div dangerouslySetInnerHTML={{ __html: iconListItem(iconListItemData) }} />
);
