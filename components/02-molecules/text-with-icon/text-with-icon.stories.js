import React from 'react';

import textWithIcon from './text-with-icon.twig';

import textWithIconData from './text-with-icon.yml';
import linkWithIconData from './link-with-icon.yml';
import linkWithCircledIconData from './link-with-circled-icon.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Text with Icon' };

export const defaultTextWithIcon = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: textWithIcon(textWithIconData),
    }}
  />
);
export const linkWithIcon = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: textWithIcon(linkWithIconData),
    }}
  />
);
export const linkWithCircledIcon = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: textWithIcon(linkWithCircledIconData),
    }}
  />
);
