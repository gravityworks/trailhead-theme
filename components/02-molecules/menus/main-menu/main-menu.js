class MainMenu {
  constructor(component) {
    this.component = component;
    this.topMenuItems = this.component.querySelectorAll(
      '.main-nav > .main-menu > li > a',
    );
    // this.menuItems = this.component.querySelectorAll('.main-menu__item');
    this.menuLinks = this.component.querySelectorAll('.main-menu__link');
    // this.subMenus = this.component.querySelectorAll('.sub-menu-wrap');
    this.init();
  }

  init = () => {
    // Identify the top level items so they can be treated differently
    this.topMenuItems.forEach((item) => {
      item.classList.add('top');
    });
    // Add Event Listeners
    this.menuLinks.forEach((link) => {
      link.addEventListener('click', this.onLinkClick);
    });

    // Close menu when Escape key is pressed.
    document.addEventListener('keydown', (e) => {
      const isTabPressed = e.key === 'Escape';

      if (!isTabPressed) {
        return;
      }

      this.closeAll();
    });
  };

  closeAll = () => {
    // Remove classes that open menu from all relevant elements.
    const activeItems = this.component.querySelectorAll('.active');
    const menu = document.getElementById('main-nav');
    const toggleButton = document.getElementById('toggle-expand');
    activeItems.forEach((item) => {
      item.classList.remove('active');
    });
    toggleButton.classList.remove('toggle-expand--open');
    menu.classList.remove('main-nav--open-drawer');
    document.body.classList.remove('menu-open');
  };

  closeSiblings = (link) => {
    // Traverse UP to <ul>, remove .active from ALL children elements IF they don’t have the class of .top
    const activeItems = link.parentElement.parentElement.querySelectorAll(
      '.active',
    );
    activeItems.forEach((item) => {
      item.classList.remove('active');
    });
  };

  toggleSubMenu = (link) => {
    // Traverse UP ONE LEVEL to parent, then find FIRST .sub-menu-wrap and add .active class
    const ul = link.parentElement;
    link.classList.toggle('active');
    ul.querySelector('.sub-menu-wrap').classList.toggle('active');
  };

  onLinkClick = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const link = e.currentTarget;
    let openMenu = true;
    // Handle sub-menus if present, otherwise just follow the link
    if (link.classList.contains('main-menu__link--with-sub')) {
      if (link.classList.contains('active')) {
        openMenu = false;
        this.toggleSubMenu(link);
      } else if (link.classList.contains('top')) {
        this.closeAll();
      } else {
        this.closeSiblings(link);
      }
      if (openMenu) {
        this.toggleSubMenu(link);
      }
    } else {
      window.location.href = link.href;
    }
  };
}

Drupal.behaviors.mainMenu = {
  attach(context) {
    const mainMenu = context.querySelectorAll('.main-nav');
    mainMenu.forEach((menu) => new MainMenu(menu));

    const toggleExpand = context.getElementById('toggle-expand');
    const menu = context.getElementById('main-nav');
    const menuWrapper = context.getElementById('main-menu__nav-wrapper');
    const otherElements = context.querySelectorAll('body > *:not(.main-nav)');

    function focusTrap(modalName) {
      const focusableElements =
        '#toggle-expand, button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
      const firstFocusableElement = modalName.querySelectorAll(
        focusableElements,
      )[0];
      const focusableContent = modalName.querySelectorAll(focusableElements);
      const lastFocusableElement =
        focusableContent[focusableContent.length - 1];

      document.addEventListener('keydown', (e) => {
        const isTabPressed = e.key === 'Tab';

        if (!isTabPressed) {
          return;
        }

        if (e.shiftKey) {
          if (document.activeElement === firstFocusableElement) {
            lastFocusableElement.focus();
            e.preventDefault();
          }
        } else if (document.activeElement === lastFocusableElement) {
          firstFocusableElement.focus();
          e.preventDefault();
        }
      });
      firstFocusableElement.focus();
    }

    if (menu) {
      // Show/hide mobile menu drawer.
      toggleExpand.addEventListener('click', (e) => {
        toggleExpand.classList.toggle('toggle-expand--open');
        menu.classList.toggle('main-nav--open-drawer');
        context.body.classList.toggle('menu-open');
        focusTrap(menuWrapper);
        otherElements.forEach((otherElement) => {
          // otherElement.setAttribute('inert', '');
          otherElement.setAttribute('aria-hidden', 'true');
        });
        e.preventDefault();
      });
    }
  },
};
