import React from 'react';

import breadcrumb from './breadcrumbs.twig';

import breadcrumbsData from './breadcrumbs.yml';
import breadcrumbsCapsData from './caps.yml';
import breadcrumbsHomeIconData from './home-icon.yml';
import breadcrumbsAltSlantData from './alt-slant.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Menus/Breadcrumbs' };

export const breadcrumbs = () => (
  <div dangerouslySetInnerHTML={{ __html: breadcrumb(breadcrumbsData) }} />
);
export const caps = () => (
  <div dangerouslySetInnerHTML={{ __html: breadcrumb(breadcrumbsCapsData) }} />
);
export const homeIcon = () => (
  <div
    dangerouslySetInnerHTML={{ __html: breadcrumb(breadcrumbsHomeIconData) }}
  />
);
export const altSlant = () => (
  <div
    dangerouslySetInnerHTML={{ __html: breadcrumb(breadcrumbsAltSlantData) }}
  />
);
