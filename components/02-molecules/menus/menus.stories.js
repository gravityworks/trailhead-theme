import React from 'react';

import inlineMenu from './inline/inline-menu.twig';
import mainMenu from './main-menu/main-menu.twig';
import socialMenu from './social/social-menu.twig';
import footerMenu from './footer/footer-menu.twig';
import upperMenu from './upper/upper-menu.twig';
import sidebarMenu from './sidebar/sidebar-menu.twig';

import inlineMenuData from './inline/inline-menu.yml';
import mainMenuData from './main-menu/main-menu.yml';
import socialMenuData from './social/social-menu.yml';
import footerMenuData from './footer/footer-menu.yml';
import upperMenuData from './upper/upper-menu.yml';
import sidebarMenuData from './sidebar/sidebar-menu.yml';

import './main-menu/main-menu';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Menus' };

export const inline = () => (
  <div dangerouslySetInnerHTML={{ __html: inlineMenu(inlineMenuData) }} />
);
export const main = () => (
  <div dangerouslySetInnerHTML={{ __html: mainMenu(mainMenuData) }} />
);
export const social = () => (
  <div dangerouslySetInnerHTML={{ __html: socialMenu(socialMenuData) }} />
);
export const footer = () => (
  <div dangerouslySetInnerHTML={{ __html: footerMenu(footerMenuData) }} />
);
export const upper = () => (
  <div dangerouslySetInnerHTML={{ __html: upperMenu(upperMenuData) }} />
);
export const sidebar = () => (
  <div dangerouslySetInnerHTML={{ __html: sidebarMenu(sidebarMenuData) }} />
);
