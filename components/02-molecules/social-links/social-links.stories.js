import React from 'react';

import socLinks from './social-links.twig';

import socialLinksData from './social-links.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Social Links' };

export const socialLinks = () => (
  <div dangerouslySetInnerHTML={{ __html: socLinks(socialLinksData) }} />
);
