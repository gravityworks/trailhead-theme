import React from 'react';

import ssb from './social-share-box.twig';

import ssbData from './social-share-box.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Social Share Box' };

export const socialShareBox = () => (
  <div dangerouslySetInnerHTML={{ __html: ssb(ssbData) }} />
);
