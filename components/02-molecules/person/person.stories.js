import React from 'react';

import person from './person.twig';
import headshotData from './headshot.yml';
import cardData from './card.yml';
import stripeData from './stripe.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/People' };

export const headshot = () => (
  <div dangerouslySetInnerHTML={{ __html: person(headshotData) }} />
);
export const card = () => (
  <div dangerouslySetInnerHTML={{ __html: person(cardData) }} />
);
export const stripe = () => (
  <div dangerouslySetInnerHTML={{ __html: person(stripeData) }} />
);
