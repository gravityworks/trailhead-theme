Drupal.behaviors.accordions = {
  attach(context) {
    const accordions = Array.from(context.querySelectorAll('[accordion]'));
    const titles = [];

    function toggleAccordion() {
      const title = this;
      const item = this.closest('.accordion__item');
      const accordion = this.closest('[accordion]');
      const otherItems = Array.from(accordion.children).filter(
        (child) => child !== item,
      );
      const button = title.querySelector('button');
      const isOpen = !item.classList.contains('open');
      item.classList.toggle('open');
      if (isOpen) {
        button.setAttribute('aria-expanded', 'true');
      } else {
        button.setAttribute('aria-expanded', 'false');
      }

      otherItems.forEach((otherItem) => {
        otherItem.classList.remove('open');
        otherItem
          .querySelector('.accordion__title button')
          .setAttribute('aria-expanded', 'false');
      });
    }

    accordions.forEach((accordion) => {
      Array.from(accordion.children).forEach((item) => {
        if (item.classList.contains('accordion__item')) {
          titles.push(item.querySelector('.accordion__title'));
        }
      });
    });

    titles.forEach((title) => {
      const inner = title.innerHTML;
      const contentIsVisible = title
        .closest('.accordion__item')
        .querySelector('.accordion__content')
        .getBoundingClientRect().height;
      if (contentIsVisible) {
        title.innerHTML = `<button aria-expanded="true">${inner}</button>`; // eslint-disable-line no-param-reassign
      } else {
        title.innerHTML = `<button aria-expanded="false">${inner}</button>`; // eslint-disable-line no-param-reassign
      }
      title
        .querySelector('button')
        .addEventListener('click', toggleAccordion.bind(title));
    });
  },
};
