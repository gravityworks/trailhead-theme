import React from 'react';

import accordion from './accordion.twig';

import accordionData from './accordion.yml';
import alertData from './alert.yml';

import './accordion';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Accordion' };

export const accordionExample = () => (
  <div dangerouslySetInnerHTML={{ __html: accordion(accordionData) }} />
);
export const alertExample = () => (
  <div dangerouslySetInnerHTML={{ __html: accordion(alertData) }} />
);
