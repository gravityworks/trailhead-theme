import React from 'react';

import blockquoteTwig from './blockquote.twig';
import blockquoteData from './blockquote.yml';

import profileTwig from './profile.twig';
import profileData from './profile.yml';

import plainTwig from './plain.twig';
import plainData from './plain.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Testimonials' };

export const blockquote = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquoteTwig(blockquoteData) }} />
);
export const profile = () => (
  <div dangerouslySetInnerHTML={{ __html: profileTwig(profileData) }} />
);
export const plain = () => (
  <div dangerouslySetInnerHTML={{ __html: plainTwig(plainData) }} />
);
