import React from 'react';

import numberBlockTwig from './number-block.twig';
import numberBlockBoxedData from './number-block.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Number Blocks' };

export const numberBlock = () => (
  <div
    dangerouslySetInnerHTML={{ __html: numberBlockTwig(numberBlockBoxedData) }}
  />
);
