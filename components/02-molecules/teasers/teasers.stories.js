import React from 'react';

import withDate from './with-date.twig';
import withDateSummary from './with-date-summary.twig';
import publication from './publication.twig';
import overlay from './overlay.twig';
import featured from './featured.twig';
import productMini from './product-mini.twig';

import teaserData from './teaser.yml';
import teaserFeaturedData from './featured.yml';
import teaserFeaturedDarkData from './featured-dark.yml';
import teaserFeaturedAltData from './featured-alt.yml';
import productMiniData from './product-mini.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Teasers' };

export const teaserWithDate = () => (
  <div dangerouslySetInnerHTML={{ __html: withDate(teaserData) }} />
);
export const teaserWithDateAndSummary = () => (
  <div dangerouslySetInnerHTML={{ __html: withDateSummary(teaserData) }} />
);
export const teaserPublication = () => (
  <div dangerouslySetInnerHTML={{ __html: publication(teaserData) }} />
);
export const teaserOverlay = () => (
  <div dangerouslySetInnerHTML={{ __html: overlay(teaserData) }} />
);
export const teaserFeatured = () => (
  <div dangerouslySetInnerHTML={{ __html: featured(teaserFeaturedData) }} />
);
export const teaserFeaturedDark = () => (
  <div dangerouslySetInnerHTML={{ __html: featured(teaserFeaturedDarkData) }} />
);
export const teaserFeaturedAlt = () => (
  <div dangerouslySetInnerHTML={{ __html: featured(teaserFeaturedAltData) }} />
);
export const teaserProductMini = () => (
  <div dangerouslySetInnerHTML={{ __html: productMini(productMiniData) }} />
);
