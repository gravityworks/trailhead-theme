import React from 'react';

import iconBlock from './icon-block.twig';
import iconBlockBoxedData from './boxed.yml';
import iconBlockBoxedIconData from './boxed-icon.yml';
import iconBlockStripedData from './striped.yml';
import iconBlockHorizontalData from './horizontal.yml';
import iconBlockSolidData from './solid.yml';
import iconBlockLargeData from './large.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Icon Blocks' };

export const boxed = () => (
  <div dangerouslySetInnerHTML={{ __html: iconBlock(iconBlockBoxedData) }} />
);
export const boxedIcon = () => (
  <div
    dangerouslySetInnerHTML={{ __html: iconBlock(iconBlockBoxedIconData) }}
  />
);
export const striped = () => (
  <div dangerouslySetInnerHTML={{ __html: iconBlock(iconBlockStripedData) }} />
);
export const horizontal = () => (
  <div
    dangerouslySetInnerHTML={{ __html: iconBlock(iconBlockHorizontalData) }}
  />
);
export const solid = () => (
  <div dangerouslySetInnerHTML={{ __html: iconBlock(iconBlockSolidData) }} />
);
export const large = () => (
  <div dangerouslySetInnerHTML={{ __html: iconBlock(iconBlockLargeData) }} />
);
