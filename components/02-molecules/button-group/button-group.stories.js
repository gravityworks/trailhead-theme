import React from 'react';

import group from './button-group.twig';

import buttonGroupData from './button-group.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Button Group' };

export const buttonGroup = () => (
  <div dangerouslySetInnerHTML={{ __html: group(buttonGroupData) }} />
);
