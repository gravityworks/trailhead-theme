import React from 'react';

import searchResult from './search-result.twig';
import searchResultData from './search-result.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Search Result' };

export const result = () => (
  <div dangerouslySetInnerHTML={{ __html: searchResult(searchResultData) }} />
);
