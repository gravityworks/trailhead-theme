import React from 'react';

import circledIcon from './circled-icon.twig';
import circledIconData from './circled-icon.yml';
import circledIconSolidData from './circled-icon-solid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Circled Icon' };

export const icon = () => (
  <div dangerouslySetInnerHTML={{ __html: circledIcon(circledIconData) }} />
);

export const solidIcon = () => (
  <div
    dangerouslySetInnerHTML={{ __html: circledIcon(circledIconSolidData) }}
  />
);
