import React from 'react';

import checkerboardPiece from './checkerboard-piece.twig';
import checkerboardPieceData from './checkerboard-piece.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Molecules/Checkerboard Piece' };

export const piece = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: checkerboardPiece(checkerboardPieceData),
    }}
  />
);
