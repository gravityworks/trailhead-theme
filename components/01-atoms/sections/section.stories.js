import React from 'react';

import section from './section.twig';

import sectionPrimaryData from './section-primary.yml';
import sectionSecondaryData from './section-secondary.yml';
import sectionNeutralLightData from './section-neutral-light.yml';
import sectionNeutralDarkData from './section-neutral-dark.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Sections' };

export const primarySection = () => (
  <div dangerouslySetInnerHTML={{ __html: section(sectionPrimaryData) }} />
);
export const secondarySection = () => (
  <div dangerouslySetInnerHTML={{ __html: section(sectionSecondaryData) }} />
);
export const neutralLightSection = () => (
  <div dangerouslySetInnerHTML={{ __html: section(sectionNeutralLightData) }} />
);
export const neutralDarkSection = () => (
  <div dangerouslySetInnerHTML={{ __html: section(sectionNeutralDarkData) }} />
);
