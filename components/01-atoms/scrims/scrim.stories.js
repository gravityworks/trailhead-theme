import React from 'react';

import scrim from './scrim.twig';

import scrimData from './scrim.yml';
import scrimTeaserData from './scrim-teaser.yml';
import scrimTranslucentData from './scrim-translucent.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Scrims' };

export const defaultScrim = () => (
  <div dangerouslySetInnerHTML={{ __html: scrim(scrimData) }} />
);
export const teaserScrim = () => (
  <div dangerouslySetInnerHTML={{ __html: scrim(scrimTeaserData) }} />
);
export const translucentScrim = () => (
  <div dangerouslySetInnerHTML={{ __html: scrim(scrimTranslucentData) }} />
);
