import React from 'react';

import heading from './headings/_heading.twig';
import blockquote from './text/02-blockquote.twig';
import pre from './text/05-pre.twig';
import paragraph from './text/03-inline-elements.twig';
import linedTextTwig from './text/07-lined-text.twig';

import blockquoteLinedData from './text/blockquote-lined.yml';
import blockquoteDoubleLinedData from './text/blockquote-double-lined.yml';
import blockquoteBoxedData from './text/blockquote-boxed.yml';
import blockquoteIndentedData from './text/blockquote-indented.yml';
import blockquotePlainData from './text/blockquote-plain.yml';
import headingData from './headings/headings.yml';
import linedTextData from './text/lined-text.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Text' };

// Loop over items in headingData to show each one in the example below.
const headings = headingData.map((d) => heading(d)).join('');

export const headingsExamples = () => (
  <div dangerouslySetInnerHTML={{ __html: headings }} />
);
export const blockquoteLined = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquote(blockquoteLinedData) }} />
);
export const blockquoteDoubleLined = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquote(blockquoteDoubleLinedData) }}
  />
);
export const blockquoteBoxed = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquote(blockquoteBoxedData) }} />
);
export const blockquoteIndented = () => (
  <div
    dangerouslySetInnerHTML={{ __html: blockquote(blockquoteIndentedData) }}
  />
);
export const blockquotePlain = () => (
  <div dangerouslySetInnerHTML={{ __html: blockquote(blockquotePlainData) }} />
);
export const preformatted = () => (
  <div dangerouslySetInnerHTML={{ __html: pre({}) }} />
);
export const random = () => (
  <div dangerouslySetInnerHTML={{ __html: paragraph({}) }} />
);
export const linedText = () => (
  <div dangerouslySetInnerHTML={{ __html: linedTextTwig(linedTextData) }} />
);
