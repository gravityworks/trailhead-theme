import React from 'react';

import button from './twig/button.twig';

import buttonData from './twig/button.yml';
import buttonSecondaryData from './twig/button-secondary.yml';
import buttonLargeData from './twig/button-large.yml';
import buttonGhostData from './twig/button-ghost.yml';
import buttonFullWidthData from './twig/button-full-width.yml';
import buttonWithIconData from './twig/button-with-icon.yml';
import buttonWithIconTransparentData from './twig/button-with-icon-transparent.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Atoms/Button' };

export const twig = () => (
  <div dangerouslySetInnerHTML={{ __html: button(buttonData) }} />
);
export const twigSecondary = () => (
  <div dangerouslySetInnerHTML={{ __html: button(buttonSecondaryData) }} />
);
export const twigLarge = () => (
  <div dangerouslySetInnerHTML={{ __html: button(buttonLargeData) }} />
);
export const twigGhost = () => (
  <div dangerouslySetInnerHTML={{ __html: button(buttonGhostData) }} />
);
export const fullWidth = () => (
  <div dangerouslySetInnerHTML={{ __html: button(buttonFullWidthData) }} />
);
export const withIcon = () => (
  <div dangerouslySetInnerHTML={{ __html: button(buttonWithIconData) }} />
);
export const withIconTransparent = () => (
  <div
    dangerouslySetInnerHTML={{ __html: button(buttonWithIconTransparentData) }}
  />
);
