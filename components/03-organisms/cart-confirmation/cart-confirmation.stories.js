import React from 'react';

import cartConfirmation from './cart-confirmation.twig';
import cartConfirmationData from './cart-confirmation.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Cart Confirmation' };

export const confirmation = () => (
  <div
    dangerouslySetInnerHTML={{ __html: cartConfirmation(cartConfirmationData) }}
  />
);
