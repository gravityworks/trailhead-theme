class PhotoGallery {
  constructor(el) {
    this.wrapper = el;
    this.photos = el.querySelectorAll('.photo');
    this.closeBtn = el.querySelector('.close-btn');
    this.lightbox = el.querySelector('.lightbox');
    this.figure = el.querySelector('.figure');
    this.navButton = el.querySelectorAll('.previous-next-navigation__side');
    this.prevButton = el.querySelector('.prev-btn');
    this.nextButton = el.querySelector('.next-btn');
    // this.prevButton = e.querySelector('.previous-next-navigation__')
    this.figureCaption = this.figure.querySelector('figcaption');
    this.figureImage = this.figure.querySelector('img');
    this.currentIndex = -1;
    this.numPhotos = this.photos.length;
    this.init();
  }

  init = () => {
    this.photos.forEach((photo) => {
      photo.addEventListener('click', this.onPhotoClick);
    });
    this.closeBtn.addEventListener('click', this.onCloseClick);
    this.navButton.forEach((button) => {
      button.addEventListener('click', this.onNavButtonClick);
    });
    this.lightbox.addEventListener('click', this.onLightboxClick);
    this.figure.addEventListener('click', this.onFigureClick);
  };

  onPhotoClick = (e) => {
    e.stopPropagation();
    this.showPhoto(e.target.dataset.index);
  };

  onLightboxClick = (e) => {
    e.stopPropagation();
    this.onCloseClick();
  };

  onFigureClick = (e) => {
    e.stopPropagation();
  };

  showPhoto = (index) => {
    this.currentIndex = index;
    const currentPhoto = this.photos[index].querySelector('img');
    this.figureImage.src = currentPhoto.dataset.image;
    this.figureImage.alt = currentPhoto.alt;
    this.figureCaption.innerHTML = currentPhoto.dataset.caption;
    this.lightbox.classList.remove('hidden');
    this.checkNavButtons();
  };

  onCloseClick = () => {
    this.currentIndex = -1;
    this.figureImage.src = '';
    this.figureImage.alt = '';
    this.figureCaption.innerHTML = '';
    this.lightbox.classList.add('hidden');
  };

  onNextImage = () => {
    if (this.currentIndex >= 0 && this.currentIndex < this.numPhotos - 1) {
      this.showPhoto(parseInt(this.currentIndex, 10) + 1);
    }
  };

  onPrevImage = () => {
    if (this.currentIndex > 0 && this.currentIndex <= this.numPhotos - 1) {
      this.showPhoto(parseInt(this.currentIndex, 10) - 1);
    }
  };

  onNavButtonClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    const classes = e.currentTarget.classList;
    if (classes.contains('previous-next-navigation__next')) {
      this.onNextImage();
    }
    if (classes.contains('previous-next-navigation__prev')) {
      this.onPrevImage();
    }
  };

  checkNavButtons = () => {
    this.prevButton.classList.remove('hidden');
    this.nextButton.classList.remove('hidden');
    // console.log(this.currentIndex, this.numPhotos);
    if (this.currentIndex <= 0) {
      this.prevButton.classList.add('hidden');
    }
    if (this.currentIndex >= this.numPhotos - 1) {
      this.nextButton.classList.add('hidden');
    }
  };
}

Drupal.behaviors.photoGallery = {
  attach(context) {
    const photoGalleryElements = context.querySelectorAll('.photo-gallery');
    photoGalleryElements.forEach(
      (photoGallery) => new PhotoGallery(photoGallery),
    );
  },
};
