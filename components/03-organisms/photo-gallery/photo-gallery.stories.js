import React from 'react';

import photoGalleryTwig from './photo-gallery.twig';
import photoGalleryData from './photo-gallery.yml';

import './photo-gallery';

/**
 * Storybook Definintion
 */
export default { title: 'Organisms/Photo Gallery' };

export const photoGallery = () => (
  <div
    dangerouslySetInnerHTML={{ __html: photoGalleryTwig(photoGalleryData) }}
  />
);
