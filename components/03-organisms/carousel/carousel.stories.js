import React from 'react';
import { useEffect } from '@storybook/client-api';

import carouselTwig from './carousel.twig';
import carouselData from './carousel.yml';

import './carousel';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Carousel' };

export const carousel = () => {
  useEffect(() => Drupal.attachBehaviors(), []);
  return (
    <div
      dangerouslySetInnerHTML={{
        __html: carouselTwig(carouselData),
      }}
    />
  );
};
