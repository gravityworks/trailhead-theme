import Flickity from 'flickity';

/* eslint-disable */
Drupal.behaviors.carousel = {
  attach(context) {
    // Flickity set-up
    var flkty;
    var elem = document.querySelector('.carousel');

    do_flickity();

    // init Flickity
    function do_flickity() {
      flkty = new Flickity(elem, {
        wrapAround: true,
        groupCells: true,
        contain: true,
        cellAlign: 'left',
        pageDots: true,
        arrowShape: {"x0": 10, "x1": 60, "y1": 50, "x2": 70, "y2": 40, "x3": 30}
      });
    }
  },
};
