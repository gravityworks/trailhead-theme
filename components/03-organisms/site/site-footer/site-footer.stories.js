import React from 'react';

import centeredLinedMenuTwig from './centered-lined-menu/centered-lined-menu.twig';
import centeredWithLeftLogoTwig from './centered-with-left-logo/centered-with-left-logo.twig';
import multiMenuTwig from './multi-menu/multi-menu.twig';

import centeredLinedMenuData from './centered-lined-menu/centered-lined-menu.yml';
import multiMenuData from './multi-menu/multi-menu.yml';
import centeredWithLeftLogoData from './centered-with-left-logo/centered-with-left-logo.yml';
import footerSocial from '../../../02-molecules/social-links/social-links.yml';
import footerMenu from '../../../02-molecules/menus/footer/footer-menu.yml';
import fourItemMenu from '../../../02-molecules/menus/footer/four-item-menu.yml';

import '../../../02-molecules/menus/main-menu/main-menu';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Site/Footer' };

export const centeredLinedMenu = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: centeredLinedMenuTwig({
        ...centeredLinedMenuData,
        ...footerSocial,
        ...footerMenu,
      }),
    }}
  />
);

export const centeredWithLeftLogoMenu = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: centeredWithLeftLogoTwig({
        ...centeredWithLeftLogoData,
        ...footerSocial,
        ...footerMenu,
      }),
    }}
  />
);

export const multiMenu = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: multiMenuTwig({
        ...multiMenuData,
        ...footerSocial,
        ...fourItemMenu,
      }),
    }}
  />
);
