import React from 'react';

import standardHeader from './standard/standard.twig';
import roundedLogomarkHeader from './rounded-logomark/rounded-logomark.twig';

import standardHeaderData from './standard/standard.yml';
import roundedLogomarkHeaderData from './rounded-logomark/rounded-logomark.yml';
import breadcrumbData from '../../../02-molecules/menus/breadcrumbs/breadcrumbs.yml';
import mainMenuData from '../../../02-molecules/menus/main-menu/main-menu.yml';

import '../../../02-molecules/menus/main-menu/main-menu';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Site/Header' };

export const standard = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: standardHeader({
        ...breadcrumbData,
        ...mainMenuData,
        ...standardHeaderData,
      }),
    }}
  />
);

export const roundedLogomark = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: roundedLogomarkHeader({
        ...breadcrumbData,
        ...mainMenuData,
        ...roundedLogomarkHeaderData,
      }),
    }}
  />
);
