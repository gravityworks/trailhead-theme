import React from 'react';

import checkerboardTwig from './checkerboard.twig';
import checkerboardData from './checkerboard.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Checkerboard' };

export const checkerboard = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: checkerboardTwig(checkerboardData),
    }}
  />
);
