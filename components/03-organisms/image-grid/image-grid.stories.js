import React from 'react';

import imageGrid from './image-grid.twig';
import imageGridData from './image-grid.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Image Grid' };

export const grid = () => (
  <div dangerouslySetInnerHTML={{ __html: imageGrid(imageGridData) }} />
);
