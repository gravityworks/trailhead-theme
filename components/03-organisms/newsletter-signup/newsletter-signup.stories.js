import React from 'react';

import newsletterSignup from './newsletter-signup.twig';
import newsletterSignupData from './newsletter-signup.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Newsletter Signup' };

export const newsletterSignupCTA = () => (
  <div
    dangerouslySetInnerHTML={{ __html: newsletterSignup(newsletterSignupData) }}
  />
);
