import React from 'react';

import iconBlockCallout from './icon-block-callout.twig';
import iconBlockCalloutData from './icon-block-callout.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Icon Block Callout' };

export const callout = () => (
  <div
    dangerouslySetInnerHTML={{ __html: iconBlockCallout(iconBlockCalloutData) }}
  />
);
