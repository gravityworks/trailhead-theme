import React from 'react';

import previousNext from './previous-next-navigation.twig';
import previousNextData from './previous-next-navigation.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Previous Next Navigation' };

export const previousNextNavigation = () => (
  <div dangerouslySetInnerHTML={{ __html: previousNext(previousNextData) }} />
);
