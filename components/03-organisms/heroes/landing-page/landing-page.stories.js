import React from 'react';

import landingPageTwig from './landing-page.twig';
import bottomBoxTwig from './bottom-box.twig';
import standardContentTwig from './standard-content.twig';
import centeredContentTwig from './centered-content.twig';
import logoOnlyTwig from './logo-only.twig';
import searchTwig from './search.twig';
import narrowContentTwig from './narrow-content.twig';
import scrollIndicatorTwig from './scroll-indicator.twig';
import splitTextImageTwig from './split-text-image.twig';
import iconBlocksTwig from './icon-blocks.twig';

import imageOnlyData from './image-only.yml';
import impactfulHeadingData from './impactful-heading.yml';
import bottomBoxData from './bottom-box.yml';
import centeredContentData from './centered-content.yml';
import logoOnlyData from './logo-only.yml';
import chevronBackgroundData from './chevron-background.yml';
import slantedBackgroundData from './slanted-background.yml';
import doubleBorderedBoxData from './double-bordered-box.yml';
import translucentBoxData from './translucent-box.yml';
import textTranslucentBGsData from './text-with-translucent-bgs.yml';
import splitTextImageData from './split-text-image.yml';
import iconBlocksData from './icon-blocks.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Heroes/Landing Page' };

export const imageOnly = () => (
  <div dangerouslySetInnerHTML={{ __html: landingPageTwig(imageOnlyData) }} />
);
export const logoOnly = () => (
  <div dangerouslySetInnerHTML={{ __html: logoOnlyTwig(logoOnlyData) }} />
);
export const impactfulHeading = () => (
  <div
    dangerouslySetInnerHTML={{ __html: landingPageTwig(impactfulHeadingData) }}
  />
);
export const bottomBox = () => (
  <div dangerouslySetInnerHTML={{ __html: bottomBoxTwig(bottomBoxData) }} />
);
export const centeredContent = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: centeredContentTwig(centeredContentData),
    }}
  />
);
export const scrollIndicator = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: scrollIndicatorTwig(centeredContentData),
    }}
  />
);
export const search = () => (
  <div dangerouslySetInnerHTML={{ __html: searchTwig(centeredContentData) }} />
);
export const chevronBackground = () => (
  <div
    dangerouslySetInnerHTML={{ __html: logoOnlyTwig(chevronBackgroundData) }}
  />
);
export const slantedBackground = () => (
  <div
    dangerouslySetInnerHTML={{ __html: logoOnlyTwig(slantedBackgroundData) }}
  />
);
export const translucentBox = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: narrowContentTwig(translucentBoxData),
    }}
  />
);
export const doubleBorderedBox = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: narrowContentTwig(doubleBorderedBoxData),
    }}
  />
);
export const textWithTranslucentBackgrounds = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: standardContentTwig(textTranslucentBGsData),
    }}
  />
);
export const splitTextWithImage = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: splitTextImageTwig(splitTextImageData),
    }}
  />
);
export const iconBlocks = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: iconBlocksTwig(iconBlocksData),
    }}
  />
);
