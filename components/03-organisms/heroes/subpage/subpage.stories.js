import React from 'react';

import subpageTwig from './subpage.twig';
import subpageCTALikeTwig from './subpage-cta-like.twig';

import titleData from './title/title.yml';

import offsetTitleData from './offset-title/offset-title.yml';

import centeredContentTwig from './centered-content/centered-content.twig';
import centeredContentData from './centered-content/centered-content.yml';

import titleWithImageData from './title-with-image/title-with-image.yml';

import titleWithImageAndBreadcrumbTwig from './title-with-image-and-breadcrumb/title-with-image-and-breadcrumb.twig';
import titleWithImageAndBreadcrumbData from './title-with-image-and-breadcrumb/title-with-image-and-breadcrumb.yml';

import offsetTitleWithImageData from './offset-title-with-image/offset-title-with-image.yml';

import offsetBoxData from './offset-box/offset-box.yml';

import shapedBackgroundData from './shaped-bg/shaped-bg.yml';

import slantedBackgroundData from './slanted-bg/slanted-bg.yml';

import halfAndHalfData from './half-and-half/half-and-half.yml';

import translucentBoxData from './translucent-box/translucent-box.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Heroes/Subpage' };

export const title = () => (
  <div dangerouslySetInnerHTML={{ __html: subpageTwig(titleData) }} />
);
export const offsetTitle = () => (
  <div dangerouslySetInnerHTML={{ __html: subpageTwig(offsetTitleData) }} />
);
export const centeredContent = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: centeredContentTwig(centeredContentData),
    }}
  />
);
export const titleWithImage = () => (
  <div dangerouslySetInnerHTML={{ __html: subpageTwig(titleWithImageData) }} />
);
export const titleWithImageAndBreadcrumb = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: titleWithImageAndBreadcrumbTwig(titleWithImageAndBreadcrumbData),
    }}
  />
);
export const offsetTitleWithImage = () => (
  <div
    dangerouslySetInnerHTML={{ __html: subpageTwig(offsetTitleWithImageData) }}
  />
);
export const offsetBox = () => (
  <div
    dangerouslySetInnerHTML={{ __html: subpageCTALikeTwig(offsetBoxData) }}
  />
);
export const shapedBackground = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: subpageCTALikeTwig(shapedBackgroundData),
    }}
  />
);
export const slantedBackground = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: subpageCTALikeTwig(slantedBackgroundData),
    }}
  />
);
export const halfAndHalf = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: subpageCTALikeTwig(halfAndHalfData),
    }}
  />
);
export const translucentBox = () => (
  <div
    dangerouslySetInnerHTML={{
      __html: subpageCTALikeTwig(translucentBoxData),
    }}
  />
);
