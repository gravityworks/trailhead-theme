import React from 'react';

import iconList from './icon-list.twig';
import iconListData from './icon-list.yml';
import leftImageData from './left-image.yml';

/**
 * Storybook Definition.
 */
export default { title: 'Organisms/Icon List' };

export const list = () => (
  <div dangerouslySetInnerHTML={{ __html: iconList(iconListData) }} />
);
export const listWithImageOnLeft = () => (
  <div dangerouslySetInnerHTML={{ __html: iconList(leftImageData) }} />
);
